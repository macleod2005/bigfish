## Telepítés:
docker-compose up -d --build

docker-compose exec php-apache composer install


## Útvonalak
Api url: http://localhost:8080/graphql

GraphQL playground: http://localhost:8080/graphql-playground


## Általam készített fájlok
docker-compose.yml

apache\default.conf

php\Dockerfile

src\app\GraphQL\Directives\PhoneNumberDirective.php

src\app\Models\PhoneNumber.php

src\app\Models\User.php

src\database\factories\PhoneNumberFactory.php

src\database\factories\UserFactory.php

src\database\migrations\2021_07_20_094426_create_users_table.php

src\database\migrations\2021_07_21_090449_create_phone_numbers_table.php

src\database\seeders\DatabaseSeeder.php

src\graphql\schema.graphql

src\tests\Unit\GraphQLPhoneNumberIntegrityTest.php

src\tests\Unit\GraphQLPhoneNumberTest.php

src\tests\Unit\GraphQLUserTest.php


## Külső könyvtárak
laravel/framework

mll-lab/laravel-graphql-playground

nuwave/lighthouse

phpunit/phpunit

fakerphp/faker


## Teszt eredmények

docker-compose exec php-apache php artisan test


   PASS  Tests\Unit\GraphQLPhoneNumberIntegrityTest

  ✓ creating user has one default phone number when added more

  ✓ creating user has one default phone number when added less

  ✓ creating user has one default phone number when added one

  ✓ adding extra default number results one

  ✓ updating to more default number resulting one

  ✓ updating the default to non default resulting one

  ✓ deleting the default resulting one

  ✓ deleting the last resulting one


   PASS  Tests\Unit\GraphQLPhoneNumberTest

  ✓ query phone number

  ✓ mutator add phone number to user

  ✓ mutator update phone number on user

  ✓ mutator delete phone number from user

  ✓ mutator phone number validation


   PASS  Tests\Unit\GraphQLUserTest

  ✓ query user

  ✓ query users

  ✓ query users ordering

  ✓ mutator create user

  ✓ mutator update user

  ✓ mutator update user email validation

  ✓ mutator delete user



  Tests:  20 passed
  
  Time:   14.91s