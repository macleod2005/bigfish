<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\User;
use App\Models\PhoneNumber;
use Illuminate\Foundation\Testing\WithFaker;

class GraphQLPhoneNumberTest extends TestCase
{

    use RefreshDatabase;
    use WithFaker;

    public function testQueryPhoneNumber()
    {
        $user = User::factory()->create();
        $phone_number = PhoneNumber::factory()->create(['user_id' => $user->id]);

        $response = $this->graphQL(/** @lang GraphQL */ '
            {
                user(id: "' . $user->id . '") {
                    name
                    email
                    dateOfBirth
                    isActive
                    phone_numbers
                    {
                        phoneNumber
                        isDefault
                    }
                }
            }
        ');

        $this->assertEquals($phone_number->phoneNumber, $response->json("data.user.phone_numbers.0.phoneNumber"));
        $this->assertEquals($phone_number->isDefault, $response->json("data.user.phone_numbers.0.isDefault"));
    }

    public function testMutatorAddPhoneNumberToUser()
    {
        $user = User::factory()->create();

        $phone_number = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $is_default = true;

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            create: [
                                {
                                    phoneNumber: "' . $phone_number . '"
                                    isDefault: ' . ($is_default ? 'true' : 'false') . '
                                }
                            ]
                        }
                    }
                ){
                     id
                }
            }
        ');

        $updated_user = User::find( $user->id );

        $this->assertEquals($phone_number, $updated_user->phone_numbers[0]->phoneNumber);
        $this->assertEquals($is_default, $updated_user->phone_numbers[0]->isDefault);
    }

    public function testMutatorUpdatePhoneNumberOnUser()
    {
        $user = User::factory()->create();
        $phone_number = PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);

        $new_phone_number = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            update: [
                                {
                                    id:"' . $phone_number->id . '"
                                    phoneNumber: "' . $new_phone_number . '"
                                }
                            ]
                        }
                    }
                ){
                     id
                }
            }
        ');
        $updated_phone_number = PhoneNumber::find($phone_number->id);
        $this->assertEquals($new_phone_number, $updated_phone_number->phoneNumber);
    }


    public function testMutatorDeletePhoneNumberFromUser()
    {
        $user = User::factory()->create();
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);
        $last_phone_number = PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            delete: [' . $last_phone_number->id . ']
                        }
                    }
                ){
                     id
                }
            }
        ');

        $deleted_phone_number = PhoneNumber::find($last_phone_number->id);

        $this->assertNull($deleted_phone_number);

    }

    public function testMutatorPhoneNumberValidation()
    {
        $user = User::factory()->create();
        $phone_number = PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);

        $response  = $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            update: [
                                {
                                    id:"' . $phone_number->id . '"
                                    phoneNumber: "1231232"
                                }
                            ]
                        }
                    }
                ){
                     id
                }
            }
        ');

        $this->assertEquals('Validation failed for the field [updateUser].', $response->json("errors.0.message"));
    }

}
