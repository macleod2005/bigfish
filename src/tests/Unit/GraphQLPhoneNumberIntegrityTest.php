<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\User;
use App\Models\PhoneNumber;
use Illuminate\Foundation\Testing\WithFaker;

class GraphQLPhoneNumberIntegrityTest extends TestCase
{

    use RefreshDatabase;
    use WithFaker;

    public function testCreatingUserHasOneDefaultPhoneNumberWhenAddedMore()
    {
        $name = $this->faker->name();
        $email = $this->faker->email();
        $date_of_birth = $this->faker->date();
        $is_active = $this->faker->boolean(90);
        $phone_number_1 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $phone_number_2 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $phone_number_3 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');

        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation{
                createUser(
                    input: {
                        name: "' . $name . '"
                        email: "' . $email . '"
                        dateOfBirth: "' . $date_of_birth . '"
                        isActive: ' . ($is_active ? 'true' : 'false') . '
                        phone_numbers: {
                            create: [
                                {
                                    phoneNumber: "' . $phone_number_1 . '"
                                    isDefault: true
                                }
                                {
                                    phoneNumber: "' . $phone_number_2 . '"
                                    isDefault: true
                                }
                                {
                                    phoneNumber: "' . $phone_number_3 . '"
                                    isDefault: true
                                }
                            ]
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $this->assertEquals(1,$this->getDefaultNumberCount($response->json("data.createUser.id")));
    }

    public function testCreatingUserHasOneDefaultPhoneNumberWhenAddedLess()
    {
        $name = $this->faker->name();
        $email = $this->faker->email();
        $date_of_birth = $this->faker->date();
        $is_active = $this->faker->boolean(90);
        $phone_number_1 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $phone_number_2 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $phone_number_3 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');

        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation{
                createUser(
                    input: {
                        name: "' . $name . '"
                        email: "' . $email . '"
                        dateOfBirth: "' . $date_of_birth . '"
                        isActive: ' . ($is_active ? 'true' : 'false') . '
                        phone_numbers: {
                            create: [
                                {
                                    phoneNumber: "' . $phone_number_1 . '"
                                    isDefault: false
                                }
                                {
                                    phoneNumber: "' . $phone_number_2 . '"
                                    isDefault: false
                                }
                                {
                                    phoneNumber: "' . $phone_number_3 . '"
                                    isDefault: false
                                }
                            ]
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $this->assertEquals(1,$this->getDefaultNumberCount($response->json("data.createUser.id")));
    }

    public function testCreatingUserHasOneDefaultPhoneNumberWhenAddedOne()
    {
        $name = $this->faker->name();
        $email = $this->faker->email();
        $date_of_birth = $this->faker->date();
        $is_active = $this->faker->boolean(90);
        $phone_number_1 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $phone_number_2 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $phone_number_3 = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');

        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation{
                createUser(
                    input: {
                        name: "' . $name . '"
                        email: "' . $email . '"
                        dateOfBirth: "' . $date_of_birth . '"
                        isActive: ' . ($is_active ? 'true' : 'false') . '
                        phone_numbers: {
                            create: [
                                {
                                    phoneNumber: "' . $phone_number_1 . '"
                                    isDefault: false
                                }
                                {
                                    phoneNumber: "' . $phone_number_2 . '"
                                    isDefault: false
                                }
                                {
                                    phoneNumber: "' . $phone_number_3 . '"
                                    isDefault: true
                                }
                            ]
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $this->assertEquals(1,$this->getDefaultNumberCount($response->json("data.createUser.id")));
    }

    public function testAddingExtraDefaultNumberResultsOne()
    {
        $user = User::factory()->create();
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);

        $phone_number = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            create: [
                                {
                                    phoneNumber: "' . $phone_number . '"
                                    isDefault: true
                                }
                            ]
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $this->assertEquals(1,$this->getDefaultNumberCount($user->id));
    }

    public function testUpdatingToMoreDefaultNumberResultingOne()
    {
        $user = User::factory()->create();
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);
        $non_default_phone_number = PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            update: [
                                {
                                    id:"' . $non_default_phone_number->id . '"
                                    isDefault: true
                                }
                            ]
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $this->assertEquals(1,$this->getDefaultNumberCount($user->id));
    }

    public function testUpdatingTheDefaultToNonDefaultResultingOne()
    {
        $user = User::factory()->create();
        $default_phone_number = PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            update: [
                                {
                                    id:"' . $default_phone_number->id . '"
                                    isDefault: false
                                }
                            ]
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $this->assertEquals(1,$this->getDefaultNumberCount($user->id));
    }

    public function testDeletingTheDefaultResultingOne()
    {
        $user = User::factory()->create();
        $default_phone_number = PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);
        PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => false]);

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            delete: [' . $default_phone_number->id . ']
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $this->assertEquals(1,$this->getDefaultNumberCount($user->id));
    }

    public function testDeletingTheLastResultingOne()
    {
        $user = User::factory()->create();
        $default_phone_number = PhoneNumber::factory()->create(['user_id' => $user->id, 'isDefault' => true]);

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:"' . $user->id . '"
                        phone_numbers: {
                            delete: [' . $default_phone_number->id . ']
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');
        $this->assertEquals(1,$this->getDefaultNumberCount($user->id));
    }

    private function getDefaultNumberCount($user_id)
    {
        $default_numbers = PhoneNumber::where('user_id', $user_id)->where('isDefault', true)->get();
        return $default_numbers->count();
    }
}
