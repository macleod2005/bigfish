<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;

class GraphQLUserTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testQueryUser()
    {
        $user = User::factory()->create();

        $response = $this->graphQL(/** @lang GraphQL */ '
            {
                user(id: "' . $user->id . '") {
                    name
                    email
                    dateOfBirth
                    isActive
                }
            }
        ');

        $this->assertEquals($user->name, $response->json("data.user.name"));
        $this->assertEquals($user->email, $response->json("data.user.email"));
        $this->assertEquals($user->dateOfBirth, $response->json("data.user.dateOfBirth"));
        $this->assertEquals($user->isActive, $response->json("data.user.isActive"));
    }

    public function testQueryUsers()
    {
        $count = 10;
        $users = User::factory()->count($count)->create();

        //There should be a nicer way to do this.
        $response = $this->graphQL(/** @lang GraphQL */ '
            {
                users(first:100){
                    paginatorInfo{
                        total
                    }
                    data{
                        id
                        name
                        email
                        dateOfBirth
                        isActive
                    }
                }
            }
        ');

        $this->assertEquals($count, $response->json("data.users.paginatorInfo.total"));
        for($i=0; $i<$count; $i++){
            $this->assertEquals($users[$i]->name, $response->json("data.users.data." . $i . ".name"));
            $this->assertEquals($users[$i]->email, $response->json("data.users.data." . $i . ".email"));
            $this->assertEquals($users[$i]->dateOfBirth, $response->json("data.users.data." . $i . ".dateOfBirth"));
            $this->assertEquals($users[$i]->isActive, $response->json("data.users.data." . $i . ".isActive"));
        }

    }

    // Can be expanded with individual fields and directions
    public function testQueryUsersOrdering()
    {
        User::factory()->create(['name' => 'Chris Count']);
        User::factory()->create(['name' => 'Elrich Engels']);
        User::factory()->create(['name' => 'Artemis Alastor']);
        User::factory()->create(['name' => 'Derek Derek']);
        User::factory()->create(['name' => 'Big Ben']);

        $response = $this->graphQL(/** @lang GraphQL */ '
            {
                users(orderBy:{column:"name", order:ASC}first:10){
                    data{
                        name
                    }
                }
            }
        ');

        $names = $response->json("data.users.data.*.name");

        $this->assertSame(
            [
                'Artemis Alastor',
                'Big Ben',
                'Chris Count',
                'Derek Derek',
                'Elrich Engels',
            ],
            $names
        );
    }

    public function testMutatorCreateUser()
    {
        $name = $this->faker->name();
        $email = $this->faker->email();
        $date_of_birth = $this->faker->date();
        $is_active = $this->faker->boolean(90);
        $phone_number = '36' . $this->faker->randomElement(['20','30','50','70']) . $this->faker->regexify('[0-9]{7}');
        $is_default = true;

        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation{
                createUser(
                    input: {
                        name: "' . $name . '"
                        email: "' . $email . '"
                        dateOfBirth: "' . $date_of_birth . '"
                        isActive: ' . ($is_active ? 'true' : 'false') . '
                        phone_numbers: {
                            create: [
                                {
                                    phoneNumber: "' . $phone_number . '"
                                    isDefault: ' . ($is_default ? 'true' : 'false') . '
                                }
                            ]
                        }
                    }
                ) {
                    id
                    phone_numbers {
                        id
                    }
                }
            }
        ');

        $user = User::find($response->json("data.createUser.id"));

        $this->assertEquals($name, $user->name);
        $this->assertEquals($email, $user->email);
        $this->assertEquals($date_of_birth, $user->dateOfBirth);
        $this->assertEquals($is_active, $user->isActive);
        $this->assertEquals($phone_number, $user->phone_numbers[0]->phoneNumber);
        $this->assertEquals($is_default, $user->phone_numbers[0]->isDefault);
    }

    public function testMutatorUpdateUser()
    {
        $user = User::factory()->create();

        $updated_name = $this->faker->name();
        $updated_email = $this->faker->email();
        $updated_date_of_birth = $this->faker->date();
        $updated_is_active = $this->faker->boolean(90);

        $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:' . $user->id . '
                        name:"' . $updated_name . '"
                        email: "' . $updated_email . '"
                        dateOfBirth: "' . $updated_date_of_birth . '"
                        isActive: ' . ($updated_is_active ? 'true' : 'false') . '
                    }
                ){
                    id
                }
            }
        ');

        $updated_user = User::find($user->id);

        $this->assertEquals($updated_name, $updated_user->name);
        $this->assertEquals($updated_email, $updated_user->email);
        $this->assertEquals($updated_date_of_birth, $updated_user->dateOfBirth);
        $this->assertEquals($updated_is_active, $updated_user->isActive);

    }

    public function testMutatorUpdateUserEmailValidation()
    {
        $user = User::factory()->create();

        $updated_email = 'Definetly Not a Validy e-Mail Address';

        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation{
                updateUser(
                    input: {
                        id:' . $user->id . '
                        email: "' . $updated_email . '"
                    }
                ){
                    id
                    email
                }
            }
        ');

        $this->assertEquals('Validation failed for the field [updateUser].', $response->json("errors.0.message"));
    }

    public function testMutatorDeleteUser()
    {
        $user = User::factory()->create();
        $id = $user->id;

        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation{
                deleteUser(id:' . $id . '){
                    id
                }
            }
        ');

        $deleted_user = User::find($id);

        $this->assertNull($deleted_user);

    }
}
