<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'dateOfBirth',
        'isActive',
    ];

    public function phone_numbers(): HasMany
    {
        return $this->hasMany(PhoneNumber::class);
    }

}
