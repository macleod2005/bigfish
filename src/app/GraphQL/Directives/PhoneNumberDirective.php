<?php

namespace App\GraphQL\Directives;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

use App\Models\PhoneNumber;

class PhoneNumberDirective extends BaseDirective implements FieldMiddleware
{
    private $backup_number;

    public static function definition(): string
    {
        return /** @lang GraphQL */ <<<'GRAPHQL'
            directive @example on FIELD_DEFINITION
            GRAPHQL;
    }

    public function handleField(FieldValue $fieldValue, Closure $next): FieldValue
    {
        $resolver = $fieldValue->getResolver();

        $fieldValue->setResolver(function ($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($resolver) {

            if(isset($args['id'])){
                $this->backup_number = PhoneNumber::where('user_id', $args['id'])->orderBy('updated_at', 'desc')->pluck('phoneNumber')->first();
            }

            $result = $resolver($root, $args, $context, $resolveInfo);
            
            $this->fixDefaultNumbers($result->id);

            return $result;
        });
        return $next($fieldValue);
    }

    private function fixDefaultNumbers($user_id)
    {
        $default_numbers = PhoneNumber::where('user_id', $user_id)->where('isDefault', true)->get();
        switch ($default_numbers->count()) {
            case 0:
                $last_number = PhoneNumber::where('user_id', $user_id)->orderBy('updated_at', 'desc')->first();
                if(!is_null($last_number)){
                    $last_number->isDefault = true;
                    $last_number->save();
                } else {
                    if(isset($this->backup_number)){
                        PhoneNumber::create([
                            'user_id' => $user_id,
                            'phoneNumber' => $this->backup_number,
                            'isDefault' => true
                        ]);
                    }
                }
                break;
            case 1:
                //Everithing is good
                break;
            default:
                $last_default = PhoneNumber::where('user_id', $user_id)->where('isDefault', true)->orderBy('updated_at', 'desc')->first();
                PhoneNumber::where('user_id', $user_id)->where('isDefault', true)->where('id', '!=', $last_default->id)->orderBy('updated_at', 'desc')->update(['isDefault' => false]);
                break;
        }

    }

}